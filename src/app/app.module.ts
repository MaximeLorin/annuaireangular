import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {
  LucideAngularModule,
  Home,
  UserPlus,
  Mail,
  Gift,
  XCircle,
} from 'lucide-angular';
import { BeerComponent } from './user/user.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';

@NgModule({
  declarations: [
    AppComponent,
    BeerComponent,
    HomePageComponent,
    UserDetailPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LucideAngularModule.pick({ Home, UserPlus, Mail, Gift, XCircle }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
