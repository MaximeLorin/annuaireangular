import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Beer } from 'src/app/user/user.component';
@Component({
  selector: 'app-user-detail-page',
  templateUrl: './user-detail-page.component.html',
  styleUrls: ['./user-detail-page.component.scss'],
})
export class UserDetailPageComponent implements OnInit {
  public beer?: Beer;

  constructor(private userService: UserService, private route: ActivatedRoute) {
    let id: number = this.route.snapshot.params['id'];
    const beer = this.userService.getOneBeer(id);

    this.beer = this.userService.oneBeer;
  }
  async ngOnInit() {
    this.userService.addBeer;
    this.userService.deleteBeer;
    this.userService.getOneBeer;
  }
}
