import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

export interface Beer {
  id: number;
  pic: string;
  name: string;
  age: string;
  adress: string;
  mail: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class BeerComponent implements OnInit {
  @Input()
  beer?: Beer;
  @Output()
  deleted = new EventEmitter();

  constructor() {
    this.beer?.id,
      this.beer?.pic,
      this.beer?.name,
      this.beer?.age,
      this.beer?.adress,
      this.beer?.mail;
  }

  clickButton() {
    this.deleted.emit(this.beer);
  }

  ngOnInit(): void {}
}
