import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';
import { Beer } from '../user/user.component';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  beerArr: Beer[] = [];
  oneBeer?: Beer;

  public async addBeer() {
    let beerFetch = await axios.get('https://api.punkapi.com/v2/beers/random');
    let beerData: any = beerFetch.data[0];

    this.beerArr.push({
      id: beerData.id,
      pic: beerData.image_url,
      name: beerData.name,
      age: beerData.first_brewed,
      adress: beerData.abv + ' %',
      mail: beerData.brewers_tips,
    });
  }

  public deleteBeer(beer: Beer) {
    const index: number = this.beerArr.indexOf(beer);
    this.beerArr = this.beerArr.filter((u) => u !== beer);
  }

  public async getOneBeer(beerId: number) {
    let beerFetch = await axios.get(
      'https://api.punkapi.com/v2/beers/' + beerId
    );
    let result: Beer = beerFetch.data[0];
    this.oneBeer = result;

    console.log(this.oneBeer);
    // this.oneBeer = this.beerArr.find((u) => u.id === beerId);
    // console.log(this.oneBeer, beerId, this.beerArr);
  }
  constructor() {}
}
